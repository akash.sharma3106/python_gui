from tkinter import  *
from xlwt import Workbook
import xlrd
def submit():
    sub_root=Toplevel()
    first_root.withdraw()
    sub_root.title("SUBMIT REPORT")
    sub_root.geometry("300x400+500+200")
    sub_root.config(bg="grey")

    global userinfo

    userinfo=usrinp.get()
    
    print(userinfo)
    la =  Label(sub_root,text="Hi,  "+userinfo,fg="red",bg="grey")
    la.pack(pady=5)

    global tra
    global foo
    global ele
    global ent


    Label(sub_root, text="Transportation cost", fg="blue", bg="grey").pack(pady=10)
    tra = StringVar()
    Entry(sub_root,textvariable=tra).pack()

    Label(sub_root, text="Food cost", fg="Green", bg="grey").pack(pady=10)
    foo = StringVar()
    Entry(sub_root, textvariable=foo).pack()

    Label(sub_root, text="Electricity cost ", fg="Red", bg="grey").pack(pady=10)
    ele = StringVar()
    Entry(sub_root, textvariable=ele).pack()

    Label(sub_root, text="Entertainment cost", fg="Yellow", bg="grey").pack(pady=10)
    ent = StringVar()
    Entry(sub_root, textvariable=ent).pack()
    Button(sub_root,text="SAVE",command=data).pack()
    Button(sub_root,text="DONE",command=quit).pack()

def data():

    global mnt
    mnt = mont.get()
    global tran
    tran = tra.get()
    global fod
    fod = foo.get()
    global elc
    elc = ele.get()
    global etm
    etm = ent.get()

    print(mnt)
    print(tran)
    print(fod)
    wb = Workbook()
    sheet1 = wb.add_sheet('sheet1')


    sheet1.write(1, 0, tran)
    sheet1.write(2, 0, fod)
    sheet1.write(3, 0, elc)
    sheet1.write(4, 0, etm)
    fil=userinfo+mnt
    print(fil)
    wb.save(fil+'.xlsx')


def view():
    vew_root=Tk()
    first_root.withdraw()
    print("VIEW")
    vew_root.title("VIEW REPORT")
    vew_root.geometry("300x300+500+200")
    vew_root.config(bg="grey")
    usr=usrinp.get()
    mt=mont.get()
    fle=usr+mt
    print(fle)
    workbook = xlrd.open_workbook(fle+'.xlsx')
    sheet=workbook.sheet_by_index(0)
    tp=sheet.cell_value( 1 , 0)
    fd=sheet.cell_value( 2 , 0)
    elt=sheet.cell_value( 3 , 0)
    emt=sheet.cell_value( 4 , 0)

    Label(vew_root, text="Transportation cost = "+tp , fg="blue", bg="grey").pack(pady=10)
    Label(vew_root, text="Food cost = "+fd  , fg="green", bg="grey").pack(pady=10)
    Label(vew_root, text="Electricity cost = "+elt, fg="red", bg="grey").pack(pady=10)
    Label(vew_root, text="Transportation cost = "+emt, fg="yellow", bg="grey").pack(pady=10)

first_root=Tk()
first_root.title("Main Page")
first_root.config(background="grey")
first_root.geometry("300x390+500+200")
sb = Scrollbar(first_root)
sb.pack(side=RIGHT, fill=Y)
Label(text="User Name" ,bg="grey",fg="black").pack(pady=10)
usrinp=StringVar()
Entry(textvariable=usrinp ).pack(pady=10)
Label(text="Enter a Month",fg="blue",bg="grey").pack(pady=10)
mont=StringVar()
Entry(textvariable=mont).pack()
Button(text="SUBMIT REPORT",fg="RED", borderwidth=6, relief=RAISED,command=submit).pack(padx=80,pady=40)
Button(text="VIEW REPORT",fg="GREEN",borderwidth=6, relief=RAISED,command=view).pack(padx=80,pady=40)

first_root.mainloop()