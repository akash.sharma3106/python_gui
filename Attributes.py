from tkinter import *

root = Tk()

root.wm_iconbitmap("2.ico")
root.geometry("500x500")
root.maxsize()

root.title("My Code")
#important  Lable   option
# text = adds the text
# bd = background
# fg = foreground
# font = sets the font--  font=("Times",29,"bold")
# padx = x padding
# pady = y padding
# relief = border styling = SUNKEN, RAISED ,GROOVE, RIDGE

text_lable=Label(text="""Jeffrey Preston Bezos (/ˈbeɪzoʊs/;[\na][3] né Jorgensen; born January 12, 1964) is an American \ninternet and aerospace entrepreneur, media proprietor, and \ninvestor. He is best known as the founder, chief executive \nofficer, and president of Amazon. The first \ncenti-billionaire on the Forbes wealth index, Bezos was \nnamed the "richest man in modern history" after his net worth increased to $150 billion in July 2018.[4] In September 2018, Forbes described him as "far richer than anyone else \non the planet" as he added $1.8 billion to his net worth \nwhen Amazon became the second company in history to reach a market cap of $1 trillion.""", bg="yellow", fg="blue",padx=10, pady=10, font="Times 10", borderwidth=10, relief=SUNKEN)

# Important  Pack Option

# anchor = north , south ,west , east
# side = top , bottom left , right
# fill
# padx
# pady

text_lable.pack(side = RIGHT, fill =Y)


root.mainloop()
