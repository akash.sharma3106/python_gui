from tkinter import *

from PIL import Image, ImageTk

root=Tk()

root.geometry("600x500")
root.title("Newspaper")

f1=Frame(root, bg="red" , borderwidth=6 , relief=SUNKEN)
f2=Frame(root, bg="red" , borderwidth=6 , relief=SUNKEN)

f1.pack(side=TOP,fill=Y,pady=15)
f2.pack()
l1=Label(f1,text="Coronavirus disease 2019", font="times 15 bold")
l1.pack()
image=Image.open("download.jpeg")
photo=ImageTk.PhotoImage(image)
im=Label(f1,image=photo)
im.pack()
l2=Label(f2,text="Coronavirus disease 2019 (COVID-19) is an infectious disease caused by severe \n acute respiratory syndrome coronavirus 2 (SARS-CoV-2).[8] The disease was first identified in\n December 2019 in Wuhan, the capital of China's Hubei province, and has since spread globally, \nresulting in the ongoing 2019–20 coronavirus pandemic.[9][10] As of 2 May 2020, more than 3.34\n million cases have been reported across 187 countries and territories, resulting in more than\n 238,000 deaths. More than 1.05 million people have recovered Common symptoms include fever,\n cough, fatigue, shortness of breath, and loss of smell and taste.[5][11][12] While the\n majority of cases result in mild symptoms, some progress to viral pneumonia, multi-organ failure,\n or cytokine storm.[9][13][14] The time from exposure to onset of symptoms is typically \n around five days but may range from two to fourteen days")
l2.pack()

root.mainloop()
