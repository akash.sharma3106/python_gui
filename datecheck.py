import pandas as pd

df=pd.DataFrame("df.xlsx")
start_date = "2019-1-1"
end_date = "2019-1-31"

after_start_date = df["date"] >= start_date
before_end_date = df["date"] <= end_date
between_two_dates = after_start_date & before_end_date
filtered_dates = df.loc[between_two_dates]

print(filtered_dates)