from tkinter import *


def akash(event):
    print(f"Hello {event.x}")

root=Tk()
root.title("Events")
root.geometry("500x500")

widget = Button(root, text="Submit")
widget.pack()


widget.bind('<Button-1>',akash)
widget.bind('<Double-1>',quit)

root.mainloop()
