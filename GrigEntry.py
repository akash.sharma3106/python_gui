from tkinter import *
root = Tk()

def usrpsw():
    print(uservalue.get())
    print(passvalue.get())


root.title("Entry Widget & Grid Layout ")
root.geometry("750x600")

user = Label(root,text="Username")
password = Label(root,text ="Password" )
user.grid(row =0 ,column=20,ipady=10)
password.grid(row=1, column=20)

uservalue = StringVar()
passvalue = StringVar()

usrenrty = Entry(root, textvariable = uservalue)
pswentry = Entry(root, textvariable = passvalue)

usrenrty.grid(row=0, column=23)
pswentry.grid(row=1,column=23 )


butt=Button(text="submit" , command= usrpsw)
butt.grid(row=2 ,column=23,pady=60)


root.mainloop()